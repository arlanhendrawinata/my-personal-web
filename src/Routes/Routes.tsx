import App from "@/App";
import Homepage from "@/Pages/Home/Homepage/Homepage";
import { createBrowserRouter } from "react-router-dom";

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {path: '', element: <Homepage />}
    ]
  }
])
import { Button } from "@/components/ui/button"
import { Share } from "lucide-react"
import _Breadcrumb from './_Breadcrumb';

interface NavbarProps {

}

const Navbar = ({ }: NavbarProps) => {
  return (
    <header className="sticky top-0 z-10 h-[53px] flex justify-between items-center gap-1 border-b border-zinc-800 px-4">
      <_Breadcrumb />
      <div>
        <Button
          variant="outline"
          size="sm"
          className="ml-auto gap-1.5 text-sm bg-transparent border-zinc-700"
        >
          <Share className="size-3.5" />
          Share
        </Button>
      </div>
    </header>
  )
}

export default Navbar

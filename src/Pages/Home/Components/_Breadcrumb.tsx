import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui/breadcrumb"
import { Link } from "react-router-dom"

interface BreadcrumbProps {
  
}

const _Breadcrumb = ({}: BreadcrumbProps) => {
  return(
    <Breadcrumb>
      <BreadcrumbList>
        <BreadcrumbItem>
          <Link to={'/'}>Lann</Link>
          {/* <BreadcrumbLink href="/">Lann</BreadcrumbLink> */}
        </BreadcrumbItem>
        <BreadcrumbSeparator/>
        <BreadcrumbItem>
          <BreadcrumbPage className="text-gray-300">Home</BreadcrumbPage>
        </BreadcrumbItem>
      </BreadcrumbList>
    </Breadcrumb>
  )
}

export default _Breadcrumb

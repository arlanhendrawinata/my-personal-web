import { PropsWithChildren } from "react"
import Sidebar from "../Components/Sidebar"
import Navbar from "../Components/Navbar"

interface HomeLayoutProps extends PropsWithChildren {

}

const HomeLayout = ({ children }: HomeLayoutProps) => {
  return (
    <div className="pl-[53px] bg-zinc-950 text-gray-100 overflow-hidden">
      <Sidebar />
      <div className="flex flex-col">
        <Navbar />
        <div className="p-4">
          {children}
        </div>
      </div>
    </div>
  )
}

export default HomeLayout

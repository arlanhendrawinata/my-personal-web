import ApplicationLogoType from "@/components/ApplicationLogoType";
import HomeLayout from "../Layouts/HomeLayout"
import Marquee from "react-fast-marquee";
interface HomepageProps {

}

const Homepage = ({ }: HomepageProps) => {
  return (
    <HomeLayout>
      <div className="relative w-screen" style={{ height: 'calc(100vh - 65px - 1rem)' }}>
        <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 -mt-[53px] -ml-[53px] space-y-2 z-10">
          <ApplicationLogoType className="w-60 md:w-96 fill-gray-200" />
          <span className="block text-xs md:text-base">Code the Future: Innovation in Every Line.</span>
        </div>
        <div className="relative w-screen text-[30rem] text-zinc-900 font-extrabold italic opacity-10 -translate-y-56 z-0">
          <Marquee autoFill={true} direction="left">
            <span>Software Developer &nbsp;</span>
          </Marquee>
        </div>
        <div className="relative w-screen text-[14rem] text-zinc-900 italic opacity-10 -translate-y-full z-0">
          <Marquee autoFill={true} direction="right">
            <span>Laravel &nbsp;</span>
            <span>React Native &nbsp;</span>
            <span>React &nbsp;</span>
            <span>Typescript &nbsp;</span>
          </Marquee>
        </div>
      </div>
      {/* <span className="text-9xl tracking-wider">Software Developer</span> */}
      {/* <div className="h-screen">
        Homepage
      </div> */}
    </HomeLayout>
  )
}

export default Homepage
